MathUtils = {
    roundToPrecision: function(subject, precision) {
        return +((+subject).toFixed(precision));
    }
};

console.log(MathUtils.roundToPrecision(0.3 + 0.6, 1))