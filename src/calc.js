const { sqrt, add, format, multiply,divide,subtract } = require('mathjs')
const calc = (A,B,C) => {
    switch (C){
        case  '+' :
            const ans = add(A, B)    
           const result =  format(ans, {precision: 14})
            return parseFloat(result);
        case '-' :
            const ans4 = subtract(A, B)    
           const result4 =  format(ans4, {precision: 14})
            return parseFloat(result4);
        case  '*' :
            const ans2 = multiply(A, B)    
           const result2 =  format(ans2, {precision: 14})
            return parseFloat(result2);
        case  '/'  :
            const ans3 = divide(A, B)    
           const result3 =  format(ans3, {precision: 14})
            return parseFloat(result3);
        default :
            return ('');
    }
}
exports.calc = calc;