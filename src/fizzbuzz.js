const fizzbuzz = (num) => {
    switch (true){
        case (num % 5 == 0) && (num % 7 !== 0) :
            return ('buzz');
        case (num % 7 == 0) && (num % 5 !== 0) :
            return ('fizz');
        case (num % 7 == 0) && (num % 5 ==0) :
            return ('fizzbuzz');
        case (num == null)  :
            return ("'Error!'");
        default :
            return ('');
    }
}


exports.fizzbuzz = fizzbuzz;