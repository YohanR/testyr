
const expect = require('chai').expect;
const {calc} = require('../src/calc');



describe("calc", () => {
    it("Should return '0.9' ", () => {
        expect(calc(0.3,0.6,'+')).to.equal(0.9);
    });
    it("Should return '4' ", () => {
        expect(calc(8,4,'-')).to.equal(4);
    });
    it("Should return '8' ", () => {
        expect(calc(0.6,3,'*')).to.equal(1.8);
    });
    it("Should return '3' ", () => {
        expect(calc(12,4,'/')).to.equal(3);
    });
    it("Should return 'peut pas diviser par 0' ", () => {
        expect(calc(99,0,'/')).to.equal(Infinity);
    });
    it("Should return '6' ", () => {
        expect(calc(1.5,4,'*')).to.equal(6);
    });
    it("Should return '0' ", () => {
        expect(calc(0,1,'/')).to.equal(0);
    });
})
